<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css"
        integrity="sha512-yHknP1/AwR+yx26cB1y0cjvQUMvEa2PFzt1c9LlS4pRQ5NOTZFWbhBig+X9G9eYW/8m0/4OXNx8pxJ6z57x0dw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />

    <link rel="stylesheet" href="public/gulpstyle.css">

</head>

<body>

    <section class="bg-green py-2">

        <ul class="nav slider justify-content-center " style="font-size:0.75rem;">
            <li class="nav-item   me-2">
                <a class="nav-link text-white" href="#">
                    Sales 01727 260688
                </a>
            </li>

            <li class="nav-item  me-2">
                <a class="nav-link text-white" hef="#">
                    sales@howtradepartners.co.uk
                </a>
            </li>

            <li class="nav-item  me-2">
                <a class="nav-link text-white" href="#">
                    Trade pricing available
                </a>
            </li>

            <li class="nav-item  me-2">
                <a class="nav-link text-white " href="#">
                    2 man delivery service
                </a>
            </li>
        </ul>

    </section>
    <section class="bg-green lines sticky-lg-top">
        <div class="container py-3">

            <div class="row justify-content-between align-items-center ">
                <div class="col-6 col-lg-auto col-sm-auto order-lg-1 order-sm-1 order-first">
                    <img src="./images/nav-img.png" class="img-fluid">
                </div>
                <div class="col-lg-4 col-md-12 order-lg-2 order-sm-3 order-last ">
                    <div class="input-group me-4 w-100 ">
                        <input class="form-control border-end-0" type="text"
                            placeholder="Search for a worktop, laminates, samples or accessories">
                        <button class="btn border border-start-0"><img src="../gulp2/images/search.png"></button>
                    </div>
                </div>
                <div class="col-6 col-lg-5 col-sm-7 order-lg-3 order-sm-2 py-3 px-0 py-lg-0">
                    <ul class="nav ms-1 align-items-center justify-content-end " style="font-size:0.75rem;">
                        <li class="nav-item me-3 text-center">
                            <img src="../gulp2/images/shop.svg">
                            <br class="d-xl-none">
                            <span>Cart</span>
                        </li>
                        <li class="nav-item me-3 text-center">
                            <img src="../gulp2/images/track1.svg">
                            <br class="d-xl-none">
                            <span>Track Orders</span>
                        </li>

                        <li class="nav-item d-md-none d-lg-none d-xl-none me-3 text-center">
                            <img src="../gulp2/images/login1.svg" class=" text-white">
                            <br class="d-xl-none">
                            <span class="text-white">Login</span>
                        </li>



                        <li class="nav-item d-none d-sm-none d-md-block d-lg-block d-xl-block">
                            <button class="btn btn-light" type="button">
                                <img src="../gulp2/images/login2.svg">
                                <br class="d-xl-none">
                                My Account
                                <img src="../gulp2/images/arrow.svg" class="ms-1">
                            </button>

                        </li>
                    </ul>

                </div>
            </div>

        </div>
    </section>
    <section class="bg-green py-2">

        <div class="container test">
            
                <ul class="navbar-nav flex-row justify-content-between" style="font-size:0.75rem;">
                    <li class=" nav-item ">
                        <a class="nav-link text-white" href="#">
                            <img src="../gulp2/images/oak-nav.png" class="img-responsive  mx-auto d-xl-none">
                            Oak Worktops
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a class="nav-link text-white" href="#">
                            <img src="../gulp2/images/kitchen-nav.png" class="img-responsive  mx-auto d-xl-none">
                            Kitchen Worktops</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link text-white" href="#">
                            <img src="../gulp2/images/wood-nav.png" class="img-responsive  mx-auto d-xl-none">
                            Wooden Worktops</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link text-white" href="#">
                            <img src="../gulp2/images/laminate-nav.png" class="img-responsive  mx-auto d-xl-none">
                            Laminate Worktops</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link text-white" href="#">
                            <img src="../gulp2/images/compact-nav.png" class="img-responsive mx-auto d-xl-none">
                            Compact Laminates Worktops</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link text-white" href="#">
                            <img src="../gulp2/images/accessories-nav.png" class="img-responsive mx-auto d-xl-none">
                            Accessories</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white" href="#">
                            <img src="../gulp2/images/worktop builder-nav.png" class="img-responsive mx-auto d-xl-none">
                            Worktop Builder</a>
                    </li>
                </ul>
          
        </div>

    </section>
    <section>
        <div class="d-flex justify-content-center py-2 mb-3" style="background: #D89654;">
            <img src="../gulp2/images/stroke.png" class="me-2">
            <div class="text-center text-white " style="font-size: 0.75rem;">10% discount will be added at checkout.
            </div>
        </div>
    </section>