<?php include('./header.php');?>

<div class="container">
    <div class="row py-4 px-4">
        <div class="col-sm-12 col-lg-6 me-1">
            <div class="row">
                <div class="col-md-6 mb-4">
                    <img src="./images/oak-worktop-01.jpg" class="img-fluid" width="100%">
                </div>
                <div class="col-md-6 mb-4">
                    <img src="./images/oak-worktop-02.jpg" class="img-fluid" width="100%">
                </div>
                <div class="col-md-6 mb-4">
                    <img src="./images/oak-worktop-03.jpg" class="img-fluid" width="100%">
                </div>
                <div class="col-md-6 mb-4">
                    <img src="./images/oak-worktop-04.jpg" class="img-fluid" width="100%">
                </div>
            </div>
            
            <div class="text-center py-3">
            <button type="button" class="btn btn-outline-dark ">See more images</button>
            </div>

            <div class="accordion accordion-flush" id="accordionFlushExample">
                <div class="accordion-item border-top">
                    <h2 class="accordion-header" id="flush-headingOne">
                        <button class="accordion-button collapsed " type="button" data-bs-toggle="collapse"
                            data-bs-target="#flush-collapseOne" aria-expanded="true" aria-controls="flush-collapseOne">
                            Product Information
                        </button>
                    </h2>
                    <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne"
                        data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body">Lorem ipsum dolor sit amet. Id eaque aperiam et nihil
                            fugit et dolorem rerum in voluptatem magnam in dolores illum. Est velit eius sed
                            sunt sint rem inventore optio.</div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingTwo">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                            Finish
                        </button>
                    </h2>
                    <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo"
                        data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body">Lorem ipsum dolor sit amet. Id eaque aperiam et nihil
                            fugit et dolorem rerum in voluptatem magnam in dolores illum. Est velit eius sed
                            sunt sint rem inventore optio.</div>
                    </div>
                </div>
                <div class="accordion-item mb-5 border-bottom">
                    <h2 class="accordion-header" id="flush-headingThree">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#flush-collapseThree" aria-expanded="false"
                            aria-controls="flush-collapseThree">
                            Care Instructions
                        </button>
                    </h2>
                    <div id="flush-collapseThree" class="accordion-collapse collapse"
                        aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body">Lorem ipsum dolor sit amet. Id eaque aperiam et nihil
                            fugit et dolorem rerum in voluptatem magnam in dolores illum. Est velit eius sed
                            sunt sint rem inventore optio.</div>
                    </div>
                </div>
            </div>

            <div></div>

        </div>

        <div class="col-sm-12 col-lg-5">
            <div class="position-sticky">
                <div class="mb-5">
                    <h3>Oak Worktop</h3>
                    <p>Due to its natural beauty, warmth, and resistance to aging, Oak has ever since been one of
                        the most popular choices for wooden kitchen worktops.</p>
                </div>
                <hr >
                <button type="button" class="btn btn-lg text-white rounded w-100 ">Sign in to purchase</button>
                <hr>

                <div class="row">
                    <div class="col text-center px-1">
                        <img src="../gulp2/images/cart.svg" class="img-fluid mb-2">
                        <h6>Best Value for Money</h6>
                    </div>

                    <div class="col text-center px-1">
                        <img src="../gulp2/images/shield1.svg" class="img-fluid mb-2">
                        <h6>Uncompromised Quality</h6>
                    </div>

                    <div class="col text-center px-1">
                        <img src="../gulp2/images/stop1.svg" class="img-fluid mb-2">
                        <h6>0% Waste</h6>
                    </div>

                    <div class="col text-center px-1">
                        <img src="../gulp2/images/track2.svg" class="img-fluid mb-2">
                        <h6>2 Man Delivery</h6>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>

<?php include ('./footer.php');?>