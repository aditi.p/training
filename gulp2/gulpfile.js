
var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var autoprefix = require('gulp-autoprefixer');
var csso = require('gulp-csso');

sass.compiler = require('node-sass');

var browserSync = require('browser-sync');
var reload = browserSync.reload;


gulp.task('styles', function () {
  return gulp.src('./src/scss/gulpstyle.scss')
    .pipe(sass.sync())
    .pipe(autoprefix())
    .pipe(csso())
    .pipe(gulp.dest('./public/'))
});

gulp.task('js', function () {
  return gulp.src(['./node_modules/bootstrap/dist/js/bootstrap.min.js', './src/js/app.js' ])
	
    .pipe(concat('app.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./public/'));
});

gulp.task('serve', gulp.series('styles', function () {

  browserSync.init({
    proxy: 'localhost/training/gulp2/',
  });

  gulp.watch('./src/**/*.scss', gulp.series('styles')).on('change', reload);
  gulp.watch('./assets/js/**/*.js', gulp.series('js')).on('change', reload);
  gulp.watch("./**/*.html").on('change', reload);
}));


