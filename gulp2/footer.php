<section class="border-top">
    <div class="container">
        <footer class="page-footer pt-4">
            <div class="row">
                <div class="col-lg-6 col-sm-6 mb-4 text-center text-sm-start text-md-start text-lg-start fw-bolder">
                    <div>House of Worktop Trade Partners is brand orperating under Tron Worktops Ltd.</div>
                    <div>UK Registered Company</div>
                    <div>Company Reg No: 11934227</div>
                    <div class="mb-1">VAT Number: GB 337 9500 84</div>
                    <div class=" mb-2">
                        <img src="../gulp2/images/Combined-Shape.svg" class="me-2">
                        01727 260688
                    </div>
                    <div>
                        <img src="../gulp2/images/Mail.svg" class="me-2">
                        sales@howtradepartners.co.uk
                    </div>

                </div>
                <div class="col-lg-6 col-sm-6 col-12 text-center text-md-start">
                    <div class="row">
                        <div class="col-lg-6 col-sm col-6">
                            <h6 class="fw-bolder">Information</h6>
                            <ul class="list-unstyled">
                                <li class="list-group">Contact Us</li>
                                <li class="list-group">Delivery Option</li>
                                <li class="list-group">Shipping</li>
                                <li class="list-group">Terms & Conditions</li>
                                <li class="list-group">Privacy</li>
                            </ul>
                        </div>

                        <div class="col-lg-6 col-sm col-6">
                            <h6 class="fw-bolder">My Account</h6>
                            <ul class="list-unstyled">
                                <li class="list-group">Account</li>
                                <li class="list-group">Order History</li>
                                <li class="list-group">Track your order</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="footer-bottom py-3">
                <div class="row">
                    <div class="col-12 col-sm-6 text-center text-sm-start fw-bolder py-2">House of Worktops Trade
                        Partners © 2022</div>
                    <br>
                    <div class="col-12 col-sm-6 text-center text-sm-end py-2">
                        <span class=" fw-bolder">Powered by</span>
                        <img src="../gulp2/images/AT.png">
                    </div>
                </div>

            </div>
        </footer>
    </div>
</section>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"
    integrity="sha512-XtmMtDEcNz2j7ekrtHvOVR4iwwaD6o/FUJe6+Zq+HgcCsk3kj4uSQQR8weQ2QVj1o0Pk6PwYLohm206ZzNfubg=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>



<script>
$(document).ready(function() {

    var query_mobile = 992;
    if ($(window).width() < query_mobile) {
        $(".test ul").addClass("slide");
    } else {
        $(".test ul").removeClass("slide");
    }
    
    $('.slider').slick({
        dots: false,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1000,
        prevArrow: false,
        nextArrow: false,
        responsive: [{
                breakpoint: 992,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: true,
                    autoplay: true,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,

                }
            }
        ]
    });


    $('.slide').slick({
        dots: false,
        infinite: true,
        slidesToShow: 7,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1000,
        prevArrow: false,
        nextArrow: false,
        responsive: [

            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 2,
                    infinite: false,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,

                }
            }
        ]
    });


   



    $('.products').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        autoplay: true,
        autoplaySpeed: 1000,
        dotsClass: 'slick-dots',
        arrows: true,
        slidesToScroll: 1,
        initialSlide: 0,
        prevArrow: '.slick-prev',
        nextArrow: '.slick-next',
        responsive: [{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }

        ]
    });
});
</script>
</body>

</html>