

  <?php include('./header.php');?>
  <section class="">
    <div class="container">
      <h6 class="text-primary">SHOP BY CATEGORY</h6>
      <div class="d-flex justify-content-between">
        <div class="text2">
          <h5 class="mb-3">A wide range of materials and accessories </h5>
        </div>
      </div>



      <div class="row">
        <div class="col-lg-6 col-xl-4 col-12 col-md-6  mb-4 position-relative">
            <img src="./images/oak-worktops.jpeg" class="img-fluid">
            <button type="button" class="btn card-btn btn-light w-70 py-1 position-absolute">
              <span>Oak Worktops</span>
              <img src="./images/Group43.png">
            </button>
        </div>
        <div class="col-lg-6 col-xl-4 col-12 col-md-6 mb-4 position-relative">
          <img src="./images/wooden-worktops.jpeg" class="img-fluid">
          <button type="button" class="btn card-btn btn-light w-70 py-1 position-absolute">
            <span>Wooden Worktops</span>
            <img src="./images/Group43.png">
          </button>
        </div>

        <div class="col-lg-6 col-xl-4 col-12 col-md-6 mb-4 position-relative">
          <img src="./images/laminate-worktops.jpeg" class="img-fluid">
          <button type="button" class="btn card-btn btn-light w-70 py-1 position-absolute">
            <span>Laminate Worktops</span>
            <img src="./images/Group43.png">
          </button>
        </div>

        <div class="col-lg-6 col-xl-4 col-12 col-md-6 mb-4 position-relative">
          <img src="./images/compact-worktops.jpeg" class="img-fluid">
          <button type="button" class="btn card-btn btn-light w-70 py-1 position-absolute">
            <span>Compact Worktops</span>
            <img src="./images/Group43.png">
          </button>
        </div>

        <div class="col-lg-6 col-xl-4 col-12 col-md-6 mb-4 position-relative">
          <img src="./images/custom-worktop-builder.jpeg" class="img-fluid">
          <button type="button" class="btn card-btn btn-light w-70 py-1 position-absolute ">
            <span>Custom Worktop Builder</span>
            <img src="./images/Group43.png">
          </button>
        </div>

        <div class="col-lg-6 col-xl-4 col-12 col-md-6 mb-4 position-relative">
          <img src="./images/accessories.jpeg" class="img-fluid">
          <button type="button" class="btn card-btn btn-light w-70 py-1 position-absolute">
            <span>Accessories</span>
            <img src="./images/Group43.png">
          </button>
        </div>
      </div>
    </div>
  </section>

  <section class="py-5">
    <div class="container">
      <h6 class="text-warning">Featured</h6>
      <div class="row align-items-center">
        <div class="col-sm-6 col-md-8 mb-3">
          <h4 class="fw-bolder">Worktops that you will love </h4>
          <div class="text-grey-500">
            Browse from a range of kitchen worktops
          </div>
        </div>
        <div class="col-sm-6 col-md-4 text-end d-grid">
          <button type="button" class="btn btn-outline-dark">Explore all wooden worktops</button>
        </div>
      </div>

      <div class="row mt-3">
        <div class="col-lg-3 col-sm-6">
          <div class="card mb-3">
            <a href="./oakworktop.php">
            <img class="card-img-top" src="./images/product-image1.png">
            </a>
            <div class="card-body">
              <h6 class="card-title fw-bolder">Oak Worktop</h6>
              <h6 class="small line-height-0">FROM</h6>
              <div class="d-flex justify-content-between">
                <h4 class="price">£66</h4>
                <a href="javascript:void(0);" class="btn">Shop Now</a>
              </div>
            </div>
          </div>
          
        </div>
        <div class="col-lg-3 col-sm-6">
          <div class="card mb-3">
            <img class="card-img-top" src="./images/product-image2.png">
            <div class="card-body">
              <h6 class="card-title fw-bolder ">Walnut Worktop</h6>
              <h6 class="small line-height-0">FROM</h6>
              <div class="d-flex justify-content-between">
                <h4 class="price">£66</h4>
                <a href="javascript:void(0);" class="btn">Shop Now</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-sm-6">
          <div class="card mb-3">
            <img class="card-img-top" src="./images/product-image3.png">
            <div class="card-body">
              <h6 class="card-title fw-bolder">Beech Worktop</h6>
              <h6 class="small line-height-0">FROM</h6>
              <div class="d-flex justify-content-between">
                <h4 class="price">£66</h4>
                <a href="javascript:void(0);" class="btn">Shop Now</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-sm-6">
          <div class="card mb-3">
            <img class="card-img-top" src="./images/product-image4.png">
            <div class="card-body">
              <h6 class="card-title fw-bolder">Deluxe Rustic Oak Worktop</h6>
              <small class="small line-height-0">FROM</small>
              <div class="d-flex justify-content-between">
                <h4 class="price">£66</h4>
                <a href="#" class="btn">Shop Now</a>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>

  </section>

  <section class="py-5">
    <div class="container">
      <h6 class="text-warning">Featured</h6>
      <div class="row align-items-center">
        <div class="col-sm-6 col-md-8 mb-3">
          <h4 class="fw-bolder">Worktops that you will love </h4>
          <div class="text-grey-500">
            Browse from a range of kitchen worktops
          </div>
        </div>
        <div class="col-sm-6 col-md-4 text-end ">
        <button class="slick-prev" type="button">
         <img src="../gulp2/images/prev.svg">
        </button>
        <button class="slick-next" type="button">
        <img src="../gulp2/images/next.svg">
        </button>
        </div>
      </div>

      <div class="row mt-3 products">
      
        <div class="col-lg-3 col-sm-6">
          <div class="card mb-3">
            <a href="./oakworktop.php">
            <img class="card-img-top" src="./images/product-image1.png">
            </a>
            <div class="card-body">
              <h6 class="card-title fw-bolder">Oak Worktop</h6>
              <h6 class="small line-height-0">FROM</h6>
              <div class="d-flex justify-content-between">
                <h4 class="price">£66</h4>
                <a href="javascript:void(0);" class="btn">Shop Now</a>
              </div>
            </div>
          </div>
          
        </div>
        <div class="col-lg-3 col-sm-6">
          <div class="card mb-3">
            <img class="card-img-top" src="./images/product-image2.png">
            <div class="card-body">
              <h6 class="card-title fw-bolder ">Walnut Worktop</h6>
              <h6 class="small line-height-0">FROM</h6>
              <div class="d-flex justify-content-between">
                <h4 class="price">£66</h4>
                <a href="javascript:void(0);" class="btn">Shop Now</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-sm-6">
          <div class="card mb-3">
            <img class="card-img-top" src="./images/product-image3.png">
            <div class="card-body">
              <h6 class="card-title fw-bolder">Beech Worktop</h6>
              <h6 class="small line-height-0">FROM</h6>
              <div class="d-flex justify-content-between">
                <h4 class="price">£66</h4>
                <a href="javascript:void(0);" class="btn">Shop Now</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-sm-6">
          <div class="card mb-3">
            <img class="card-img-top" src="./images/product-image4.png">
            <div class="card-body">
              <h6 class="card-title fw-bolder">Deluxe Rustic Oak Worktop</h6>
              <small class="small line-height-0">FROM</small>
              <div class="d-flex justify-content-between">
                <h4 class="price">£66</h4>
                <a href="#" class="btn">Shop Now</a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-sm-6">
          <div class="card mb-3">
            <img class="card-img-top" src="./images/product-image4.png">
            <div class="card-body">
              <h6 class="card-title fw-bolder">Deluxe Rustic Oak Worktop</h6>
              <small class="small line-height-0">FROM</small>
              <div class="d-flex justify-content-between">
                <h4 class="price">£66</h4>
                <a href="#" class="btn">Shop Now</a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-sm-6">
          <div class="card mb-3">
            <img class="card-img-top" src="./images/product-image4.png">
            <div class="card-body">
              <h6 class="card-title fw-bolder">Deluxe Rustic Oak Worktop</h6>
              <small class="small line-height-0">FROM</small>
              <div class="d-flex justify-content-between">
                <h4 class="price">£66</h4>
                <a href="#" class="btn">Shop Now</a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-sm-6">
          <div class="card mb-3">
            <img class="card-img-top" src="./images/product-image4.png">
            <div class="card-body">
              <h6 class="card-title fw-bolder">Deluxe Rustic Oak Worktop</h6>
              <small class="small line-height-0">FROM</small>
              <div class="d-flex justify-content-between">
                <h4 class="price">£66</h4>
                <a href="#" class="btn">Shop Now</a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-sm-6">
          <div class="card mb-3">
            <img class="card-img-top" src="./images/product-image4.png">
            <div class="card-body">
              <h6 class="card-title fw-bolder">Deluxe Rustic Oak Worktop</h6>
              <small class="small line-height-0">FROM</small>
              <div class="d-flex justify-content-between">
                <h4 class="price">£66</h4>
                <a href="#" class="btn">Shop Now</a>
              </div>
            </div>
          </div>
        </div>

       

      </div>

      

    </div>

   
  </section>
 
  

  <?php include ('./footer.php');?>

